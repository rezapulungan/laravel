<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showLogin()
    {
        return view('Auth.Login.show');
    }

    public function showRegistrasi()
    {
        return view('Auth.Registrasi.show');
    }
}
